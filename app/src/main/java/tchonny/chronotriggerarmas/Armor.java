package tchonny.chronotriggerarmas;

/**
 * Created by tchonny on 01/09/2017.
 */

public class Armor extends Item {
    private String user, defense;

    public String getUser() {
        return user;
    }

    public String getDefense() {
        return defense;
    }

    public Armor(String name, String type, String damage, String ability, String whereToFind) {
        super(name, ability, whereToFind);
        this.user = type;
        this.defense = damage;

    }
}
