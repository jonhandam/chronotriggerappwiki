package tchonny.chronotriggerarmas;

/**
 * Created by tchonny on 27/10/2017.
 */

public class Character {
    private String idImage, name, time, weapon, element, description;

    public Character(String idImage, String name, String time, String weapon, String element, String description) {
        this.idImage = idImage;
        this.name = name;
        this.time = time;
        this.weapon = weapon;
        this.element = element;
        this.description = description;
    }

    public String getIdImage() {
        return idImage;
    }

    public String getName() {
        return name;
    }

    public String getTime() {
        return time;
    }

    public String getWeapon() {
        return weapon;
    }

    public String getElement() {
        return element;
    }

    public String getDescription() {
        return description;
    }
}
