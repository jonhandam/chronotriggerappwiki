package tchonny.chronotriggerarmas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class CharactersChoiceActivity extends AppCompatActivity implements View.OnClickListener{

    private ImageView chronoButton, marleButton, luccaButton, frogButton,
            roboButton, aylaButton, magusButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characters_choice_view);


        chronoButton = (ImageView) findViewById(R.id.imageChronoID);
        marleButton = (ImageView) findViewById(R.id.imageMarleID);
        luccaButton = (ImageView) findViewById(R.id.imageLuccaID);
        frogButton = (ImageView) findViewById(R.id.imageFrogID);
        roboButton = (ImageView) findViewById(R.id.imageRoboID);
        aylaButton = (ImageView) findViewById(R.id.imageAylaID);
        magusButton = (ImageView) findViewById(R.id.imageMagusID);

        chronoButton.setOnClickListener(this);
        marleButton.setOnClickListener(this);
        luccaButton.setOnClickListener(this);
        frogButton.setOnClickListener(this);
        roboButton.setOnClickListener(this);
        aylaButton.setOnClickListener(this);
        magusButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // MUDA SEGUNDO ACTIVITY OU IMPLEMENTAR TUDO NO MESMO.
        // Possivelmente será necessário um refactor aqui depois
        Intent intent = new Intent(CharactersChoiceActivity.this, ListViewActivity.class);

        Bundle extra = getIntent().getExtras();
        if (extra.get(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.SINGLE_TECH.getValue())) {
            intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.SINGLE_TECH.getValue());
        } else if (extra.get(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.DUAL_TECH.getValue())) {
            intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.DUAL_TECH.getValue());
        } else if (extra.get(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.TRIPLE_TECH.getValue())) {
            intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.TRIPLE_TECH.getValue());
        } else if (extra.get(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.WEAPON.getValue())) {
            intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.WEAPON.getValue());
        } else if (extra.get(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.CHARACTERS.getValue())){
            intent = null;
            intent = new Intent(CharactersChoiceActivity.this, CharactersViewActivity.class);
            intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.CHARACTERS.getValue());
        }

        switch (v.getId()) {
            case R.id.imageChronoID:
                intent.putExtra(ChronoTriggerID.ID_CHARACTER.getValue(), ChronoTriggerID.CHRONO.getValue());
                break;
            case R.id.imageMarleID:
                intent.putExtra(ChronoTriggerID.ID_CHARACTER.getValue(), ChronoTriggerID.MARLE.getValue());
                break;
            case R.id.imageLuccaID:
                intent.putExtra(ChronoTriggerID.ID_CHARACTER.getValue(), ChronoTriggerID.LUCCA.getValue());
                break;
            case R.id.imageFrogID:
                intent.putExtra(ChronoTriggerID.ID_CHARACTER.getValue(), ChronoTriggerID.FROG.getValue());
                break;
            case R.id.imageRoboID:
                intent.putExtra(ChronoTriggerID.ID_CHARACTER.getValue(), ChronoTriggerID.ROBO.getValue());
                break;
            case R.id.imageAylaID:
                intent.putExtra(ChronoTriggerID.ID_CHARACTER.getValue(), ChronoTriggerID.AYLA.getValue());
                break;
            case R.id.imageMagusID:
                intent.putExtra(ChronoTriggerID.ID_CHARACTER.getValue(), ChronoTriggerID.MAGUS.getValue());
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }
}
