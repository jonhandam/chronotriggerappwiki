package tchonny.chronotriggerarmas;

import java.io.Serializable;

/**
 * Created by Jonatas on 19/10/2017.
 */

public class Guide extends ChronoTriggerCommonObjetc {
    private String description;

    public Guide(String title, String description) {
        super(title);
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
