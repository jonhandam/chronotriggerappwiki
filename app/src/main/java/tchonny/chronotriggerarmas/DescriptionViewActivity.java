package tchonny.chronotriggerarmas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DescriptionViewActivity extends AppCompatActivity {

    TextView descriptionTextView1, descriptionTextView2, descriptionTextView3,
            descriptionTextView4, descriptionTextView5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_view);

        // REOLHAR DEPOIS.
        descriptionTextView1 = (TextView) findViewById(R.id.descriptionTextview1ID);
        descriptionTextView2 = (TextView) findViewById(R.id.descriptionTextview2ID);
        descriptionTextView3 = (TextView) findViewById(R.id.descriptionTextview3ID);
        descriptionTextView4 = (TextView) findViewById(R.id.descriptionTextview4ID);
        descriptionTextView5 = (TextView) findViewById(R.id.descriptionTextview5ID);

        Bundle extra = getIntent().getExtras();
        String itenType = extra.getString(ChronoTriggerID.ID.getValue());
        Intent intent = getIntent();

        if (itenType.equals(ChronoTriggerID.WEAPON.getValue())) {
            Weapon weapon = (Weapon) intent.getSerializableExtra(ChronoTriggerID.WEAPON.getValue());
            descriptionTextView1.setText("Arma\n" + weapon.getiD());
            descriptionTextView2.setText("Tipo\n" + weapon.getType());
            descriptionTextView3.setText("Dano\n" + weapon.getDamage());
            descriptionTextView4.setText("Habilidade\n" + weapon.getAbilityOrUtility());
            descriptionTextView5.setText("Onde encontrar\n" + weapon.getWhereToFind());
        } else if (itenType.equals(ChronoTriggerID.ARMOR.getValue())) {
            Armor armor = (Armor) intent.getSerializableExtra(ChronoTriggerID.ARMOR.getValue());
            descriptionTextView1.setText("Armadura\n" + armor.getiD());
            descriptionTextView2.setText("Quem pode usar\n" + armor.getUser());
            descriptionTextView3.setText("Defesa\n" + armor.getDefense());
            descriptionTextView4.setText("Habilidade\n" + armor.getAbilityOrUtility());
            descriptionTextView5.setText("Onde encontrar\n" + armor.getWhereToFind());
        } else if (itenType.equals(ChronoTriggerID.HELMET.getValue())) {
            Helmet helmet = (Helmet) intent.getSerializableExtra(ChronoTriggerID.HELMET.getValue());
            descriptionTextView1.setText("Helmet\n" + helmet.getiD());
            descriptionTextView2.setText("Quem pode usar\n" + helmet.getUser());
            descriptionTextView3.setText("Defesa\n" + helmet.getDefense());
            descriptionTextView4.setText("Habilidade\n" + helmet.getAbilityOrUtility());
            descriptionTextView5.setText("Onde encontrar\n" + helmet.getWhereToFind());
        } else if (itenType.equals(ChronoTriggerID.ACESSORY.getValue())) {
            Acessory acessory = (Acessory) intent.getSerializableExtra(ChronoTriggerID.ACESSORY.getValue());
            descriptionTextView1.setText("Acessório\n" + acessory.getiD());
            descriptionTextView2.setText("Quem pode usar\n" + acessory.getUser());
            descriptionTextView3.setText("Habilidade\n" + acessory.getAbilityOrUtility());
            descriptionTextView4.setText("Onde encontrar\n" + acessory.getWhereToFind());
        } else if (itenType.equals(ChronoTriggerID.ITEM.getValue())) {
            Item acessory = (Item) intent.getSerializableExtra(ChronoTriggerID.ITEM.getValue());
            descriptionTextView1.setText("Item\n" + acessory.getiD());
            descriptionTextView2.setText("Utilidade\n" + acessory.getAbilityOrUtility());
            descriptionTextView3.setText("Onde encontrar\n" + acessory.getWhereToFind());
        } else if (itenType.equals(ChronoTriggerID.FINALS.getValue())) {
            Finals finalDescription = (Finals) intent.getSerializableExtra(ChronoTriggerID.FINALS.getValue());
            descriptionTextView1.setText(finalDescription.getiD());
            descriptionTextView2.setText("Condições\n" + finalDescription.getConditions());
            descriptionTextView3.setText("Variações\n" + finalDescription.getVariations());
        }
    }
}
