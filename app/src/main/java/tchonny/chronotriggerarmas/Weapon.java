package tchonny.chronotriggerarmas;

/**
 * Created by tchonny on 01/09/2017.
 */

public class Weapon extends Item {
    private String type, damage;

    public Weapon(String name, String type, String damage, String ability, String whereToFind) {
        super(name, ability, whereToFind);
        this.type = type;
        this.damage = damage;
    }

    public String getType() {
        return type;
    }

    public String getDamage() {
        return damage;
    }
}
