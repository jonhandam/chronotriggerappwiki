package tchonny.chronotriggerarmas;

import java.io.Serializable;

/**
 * Created by tchonny on 05/09/2017.
 */

public class Item extends ChronoTriggerCommonObjetc {
    private String abilityOrUtility, whereToFind;

    public Item(String name, String abilityOrUtility, String whereToFind) {
        super(name);
        this.abilityOrUtility = abilityOrUtility;
        this.whereToFind = whereToFind;
    }

    public String getAbilityOrUtility() {
        return abilityOrUtility;
    }

    public String getWhereToFind() {
        return whereToFind;
    }


}
