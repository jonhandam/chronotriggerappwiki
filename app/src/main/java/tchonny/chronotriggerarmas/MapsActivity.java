package tchonny.chronotriggerarmas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class MapsActivity extends AppCompatActivity {

    ImageView mapW65mButton, mapW12kButton, mapW1000Button, mapW2300Button, mapW600Button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mapW65mButton = (ImageView) findViewById(R.id.w65MbcImageViewID);
        mapW12kButton = (ImageView) findViewById(R.id.w12KbcImageViewID);
        mapW600Button = (ImageView) findViewById(R.id.w600adImageViewID);
        mapW1000Button = (ImageView) findViewById(R.id.w1000adImageViewID);
        mapW2300Button = (ImageView) findViewById(R.id.w2300adImageViewID);

    }
}
