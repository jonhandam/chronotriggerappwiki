package tchonny.chronotriggerarmas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class HistoryActivity extends AppCompatActivity {

    TextView historyTextView, title;
    String text = "Chrono Trigger lançado pela SQUARE (atualmente SQUARE-ENIX) foi um jogo lançado" +
            "originalmente para o Super-Nintendo (SNES) em março de 1995 no Japão, e um remake " +
            "para o PlayStation foi lançado em novembro de 1999. O jogo foi desenvolvido por uma "+
            "equipe apelida de \"The Dream Team\" (equipe dos sonhos). Composta por Hironobu " +
            "Sakaguchi (produtor da série Final Fantasy), Yuji Horii (diretor da série de jogos "+
            "Dragon Quest), Akira Toriyama (criador de animes famosos, como Dragon Ball e Dr. "+
            "Slump), o produtor Kazuhiko Aoki e Nobuo Uematsu. Certos aspectos de Chrono Trigger " +
            "foram revolucionários: sistema de múltiplos finais, missões paralelas focadas no " +
            "desenvolvimento dos personagens, sistema de batalha inovador e gráficos detalhados.";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrollview);

        Bundle extra = getIntent().getExtras();
        String type = extra.getString("name");
        Intent intent = getIntent();

        if(type.equals("history")) {
            historyTextView = (TextView) findViewById(R.id.historyTextViewID);
            historyTextView.setText(text);
        } else if (type.equals("guide")) {
            historyTextView = (TextView) findViewById(R.id.historyTextViewID);
            title = (TextView) findViewById(R.id.titleDescriptionID);
            Guide guide = (Guide) intent.getSerializableExtra("guide");
            title.setText(guide.getiD());
            historyTextView.setText(guide.getDescription());
        }
    }
}
