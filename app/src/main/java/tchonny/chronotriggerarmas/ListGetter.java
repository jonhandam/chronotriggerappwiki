package tchonny.chronotriggerarmas;

import android.content.Context;
import android.content.res.TypedArray;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by tchonny on 01/09/2017.
 */

public final class ListGetter {

    private static final ListGetter INSTANCE = new ListGetter();
    private static ArrayList<Integer> englishList, portugueseList;
    // we'll be used to decide if te list gettered wi'll be english or portuguese.
    //int rawListID;

    private ListGetter() {
        initializeEnglishList();
        initializePortugueseList();
    }

    private static final void initializeEnglishList () {

    }

    private static final void initializePortugueseList () {
        portugueseList = new ArrayList<Integer>();
        portugueseList.add(R.raw.portuguese_acessories_list);
        portugueseList.add(R.raw.portuguese_armors_list);
        portugueseList.add(R.raw.portuguese_characters_list);
        portugueseList.add(R.raw.portuguese_dual_techs_list);
        portugueseList.add(R.raw.portuguese_finals_list);
        portugueseList.add(R.raw.portuguese_guide_list);
        portugueseList.add(R.raw.portuguese_helmets_list);
        portugueseList.add(R.raw.portuguese_itens_list);
        portugueseList.add(R.raw.portuguese_single_techs_list);
        portugueseList.add(R.raw.portuguese_triple_techs_list);
        portugueseList.add(R.raw.portuguese_weapons_list);
        /*portugueseList.add();
        portugueseList.add();
        portugueseList.add();
        portugueseList.add();*/
    }

    public static ListGetter getInstance() {
        return INSTANCE;
    }

    public static void setImageView(Context context, ImageView imageView, int imageArray, String imageID) {
        TypedArray img = context.getResources().obtainTypedArray(imageArray);
        imageView.setImageResource(img.getResourceId(Integer.parseInt(imageID), -1));
        img.recycle();
    }

    public static int getListID (Context context, int rawListArrayID, int rawListID) {
        TypedArray rawList = context.getResources().obtainTypedArray(rawListArrayID);
        return rawList.getResourceId(rawListID, -1);
    }



    public static final ArrayList<Armor> getArmorArrayList(Context context){
        InputStream inputStream;
        String[] data;

        int idList = portugueseList.get(ChronoTriggerListID.ARMOR.getValue());
        inputStream = context.getResources().openRawResource(idList);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<Armor> armorArrayList = new ArrayList<Armor>();

        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split(",");
                armorArrayList.add(new Armor(data[0], data[1], data[2], data[3], data[4]));
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }

        return armorArrayList;
    }

    public static final ArrayList<Helmet> getHelmetrArrayList(Context context){
        InputStream inputStream;
        String[] data;

        //
        int idList = portugueseList.get(ChronoTriggerListID.HELMET.getValue());
        inputStream = context.getResources().openRawResource(idList);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<Helmet> helmetArrayList = new ArrayList<Helmet>();

        //
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split(",");
                helmetArrayList.add(new Helmet(data[0], data[1], data[2], data[3], data[4]));
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }

        return helmetArrayList;
    }

    public static final ArrayList<Item> getItemArrayList(Context context){
        InputStream inputStream;
        String[] data;

        //
        int idList = portugueseList.get(ChronoTriggerListID.ITEM.getValue());
        inputStream = context.getResources().openRawResource(idList);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<Item> itemArrayList = new ArrayList<Item>();

        //
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split(",");
                itemArrayList.add(new Item(data[0], data[1], data[2]));
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
        return itemArrayList;
    }

    public static final ArrayList<Acessory> getAcessoryArrayList(Context context){
        InputStream inputStream;
        String[] data;
        //
        int idList = portugueseList.get(ChronoTriggerListID.ACESSORY.getValue());
        inputStream = context.getResources().openRawResource(idList);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<Acessory> acessoryArrayList = new ArrayList<Acessory>();
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split(",");
                acessoryArrayList.add(new Acessory(data[0], data[1], data[2], data[3]));
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
        return acessoryArrayList;
    }

    public static final ArrayList<Finals> getFinalsArrayList(Context context){
        InputStream inputStream;
        String[] data;

        //
        int idList = portugueseList.get(ChronoTriggerListID.FINAL.getValue());
        inputStream = context.getResources().openRawResource(idList);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<Finals> finalsArrayList = new ArrayList<Finals>();
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split(",");
                if (data[0].equals("")){}
                else {
                    finalsArrayList.add(new Finals(data[0], data[1], data[2]));
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }

        return finalsArrayList;
    }
    
    public static final ArrayList<Guide> getGuideArrayList(Context context){
        InputStream inputStream;
        String[] data;

        //
        int idList = portugueseList.get(ChronoTriggerListID.GUIDE.getValue());
        inputStream = context.getResources().openRawResource(idList);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<Guide> guideArrayList = new ArrayList<>();
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split("#");
                if (data[0].equals("")){}
                else {
                    guideArrayList.add(new Guide(data[0], data[1]));
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
        return guideArrayList;
    }

    public static final ArrayList<Weapon> getWeaponArrayList(Context context, String idCharacter){
        InputStream inputStream;
        String[] data;

        //
        int idList = portugueseList.get(ChronoTriggerListID.WEAPON.getValue());
        inputStream = context.getResources().openRawResource(idList);        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<Weapon> weaponArrayList = new ArrayList<Weapon>();
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split(",");
                if (data[0].equals(idCharacter)) {
                    weaponArrayList.add(new Weapon(data[1], data[2], data[3], data[4], data[5]));
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
        return weaponArrayList;
    }

    public static final ArrayList<Technique> getSingleTechArrayList(Context context, String idCharacter){
        InputStream inputStream;
        String[] data;

        //
        int idList = portugueseList.get(ChronoTriggerListID.SINGLE_TECH.getValue());
        inputStream = context.getResources().openRawResource(idList);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<Technique> techniqueArrayList = new ArrayList<>();
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split("#");
                if (data[8].equals(idCharacter)) {
                    techniqueArrayList.add(new Technique(data[0], data[1], data[2], data[3],
                            data[4], data[5], data[6], data[7], data[8]));
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
        return techniqueArrayList;
    }

    public static final ArrayList<Technique> getDualTechArrayList(Context context, String idCharacter){
        InputStream inputStream;
        String[] data;

        //
        int idList = portugueseList.get(ChronoTriggerListID.DUAL_TECH.getValue());
        inputStream = context.getResources().openRawResource(idList);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<Technique> dualTechArrayList = new ArrayList<>();
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split("#");
                if (data[8].equals(idCharacter) || data[9].equals(idCharacter)) {
                    //techIdImage, characters, iD, description, mp, damageType, target
                    dualTechArrayList.add(new Technique(data[0], data[1], data[2], data[3],
                            data[4], data[5], data[6], data[7], data[8] + ", " + data[9]));
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
        return dualTechArrayList;
    }

    public static final ArrayList<Technique> getTripleTechArrayList(Context context, String idCharacter){
        InputStream inputStream;
        String[] data;

        //
        int idList = portugueseList.get(ChronoTriggerListID.TRIPLE_TECH.getValue());
        inputStream = context.getResources().openRawResource(idList);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<Technique> tripleTechArrayList = new ArrayList<>();
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split("#");
                if (data[8].equals(idCharacter) || data[9].equals(idCharacter) || data[10].equals(idCharacter)) {
                    //
                    tripleTechArrayList.add(new Technique(data[0], data[1], data[2], data[3],
                            data[4], data[5], data[6], data[7], data[8] + ", " + data[9]
                            + ", " + data[10]));
                    //TripleTech(String iD, String characters, String mpsCoasts, String requirements, String target, String description, String acessory) {

                    //Personagem1#Personagem2#Personagem3#Skill#Custo MP#Skills-Base#Alvo#Efeito#Acessório
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
        return tripleTechArrayList;
    }

    public static final Character getCharacter(Context context, String idCharacter){
        InputStream inputStream;
        String[] data;

        //
        int idList = portugueseList.get(ChronoTriggerListID.CHARACTER.getValue());
        inputStream = context.getResources().openRawResource(idList);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        Character character = null;
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split("#");
                if (data[0].equals(idCharacter)) {
                    //
                    character = new Character(data[2],data[3],data[4],data[5],data[6],data[7]);
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
        return character;
    }

    public static ArrayList<Integer> getEnglishList() {
        return englishList;
    }

    public static ArrayList<Integer> getPortugueseList() {
        return portugueseList;
    }
}

