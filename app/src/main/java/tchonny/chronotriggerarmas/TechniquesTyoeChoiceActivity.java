package tchonny.chronotriggerarmas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TechniquesTyoeChoiceActivity extends AppCompatActivity implements View.OnClickListener{

    Button singleTech, dualTech, tripleTech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_techniques);

        singleTech = (Button) findViewById(R.id.singleTechButtonID);
        dualTech = (Button) findViewById(R.id.dualTechButtonID);
        tripleTech = (Button) findViewById(R.id.tripleTechButtonID);

        singleTech.setOnClickListener(this);
        dualTech.setOnClickListener(this);
        tripleTech.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Intent intent = new Intent(TechniquesTyoeChoiceActivity.this, CharactersChoiceActivity.class);

        switch (v.getId()) {
            case R.id.singleTechButtonID:
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.SINGLE_TECH.getValue());
                startActivity(intent);
                break;
            case R.id.dualTechButtonID:
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.DUAL_TECH.getValue());
                startActivity(intent);
                break;
            case R.id.tripleTechButtonID:
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.TRIPLE_TECH.getValue());
                startActivity(intent);
                break;
        }
    }
}
