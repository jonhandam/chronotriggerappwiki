package tchonny.chronotriggerarmas;

import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class CharactersViewActivity extends AppCompatActivity {

    private TextView nameView, timeView, weaponView, elementView, descriptionView;
    private ImageView characterProfileImageView, backgroundCharacterImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characters_view);

        nameView = findViewById(R.id.characterNameView);
        timeView = findViewById(R.id.characterTimeView);
        weaponView = findViewById(R.id.characterWeaponView);
        elementView = findViewById(R.id.characterElementView);
        descriptionView = findViewById(R.id.characterDescriptionView);
        characterProfileImageView = findViewById(R.id.characterImageView);
        backgroundCharacterImageView = findViewById(R.id.backgroundCharacterImageView);

        createCharacterView();
    }

    protected void createCharacterView() {

        Bundle extra = getIntent().getExtras();
        Character character = ListGetter.getCharacter(this, extra.getString(ChronoTriggerID.ID_CHARACTER.getValue()));
        nameView.setText(character.getName());
        timeView.setText(character.getTime());
        weaponView.setText(character.getWeapon());
        elementView.setText(character.getElement());
        descriptionView.setText(character.getDescription());

        ListGetter.setImageView(this, characterProfileImageView, R.array.character_profile, character.getIdImage());
        ListGetter.setImageView(this, backgroundCharacterImageView, R.array.background_character, character.getIdImage());
    }
}
