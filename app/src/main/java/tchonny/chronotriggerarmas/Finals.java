package tchonny.chronotriggerarmas;

import java.io.Serializable;

/**
 * Created by Jonatas on 19/10/2017.
 */

public class Finals extends ChronoTriggerCommonObjetc {
    private String conditions, variations;

    public Finals(String title, String conditions, String variations) {
        super(title);
        this.conditions = conditions;
        this.variations = variations;
    }

    public String getConditions() {
        return conditions;
    }

    public String getVariations() {
        return variations;
    }
}
