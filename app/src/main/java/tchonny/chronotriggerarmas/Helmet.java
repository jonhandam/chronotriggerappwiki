package tchonny.chronotriggerarmas;

/**
 * Created by tchonny on 01/09/2017.
 */

public class Helmet extends Item {
    private String user, defense;


    public Helmet(String name, String ability, String whereToFind, String user, String defense) {
        super(name, ability, whereToFind);
        this.user = user;
        this.defense = defense;
    }

    public String getUser() {
        return user;
    }

    public String getDefense() {
        return defense;
    }
}