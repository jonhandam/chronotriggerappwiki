package tchonny.chronotriggerarmas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ItensListsActivity extends AppCompatActivity implements View.OnClickListener{

    Button weaponButton, armorButton, elmoButton, acessoryButton, itemButton, monsterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lists);

        weaponButton = (Button) findViewById(R.id.weaponButtonID);
        armorButton = (Button) findViewById(R.id.armorButtonID);
        elmoButton = (Button) findViewById(R.id.elmoButtonID);
        acessoryButton = (Button) findViewById(R.id.acessoryButtonID);
        itemButton = (Button) findViewById(R.id.itemButtonID);
        monsterButton = (Button) findViewById(R.id.itemButtonID);

        weaponButton.setOnClickListener(this);
        armorButton.setOnClickListener(this);
        elmoButton.setOnClickListener(this);
        acessoryButton.setOnClickListener(this);
        itemButton.setOnClickListener(this);
        monsterButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int choosedButtonID = v.getId();
        Intent intent;
        switch (choosedButtonID) {
            case R.id.weaponButtonID:
                intent = new Intent(ItensListsActivity.this, CharactersChoiceActivity.class);
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.WEAPON.getValue());
                break;
            case R.id.armorButtonID:
                intent = new Intent(ItensListsActivity.this, ListViewActivity.class);
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.ARMOR.getValue());
                break;
            case R.id.elmoButtonID:
                intent = new Intent(ItensListsActivity.this, ListViewActivity.class);
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.HELMET.getValue());
                break;
            case R.id.acessoryButtonID:
                intent = new Intent(ItensListsActivity.this, ListViewActivity.class);
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.ACESSORY.getValue());
                break;
            case R.id.itemButtonID:
                intent = new Intent(ItensListsActivity.this, ListViewActivity.class);
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.ITEM.getValue());
                break;
            case R.id.monsterButtonID:
                intent = new Intent(ItensListsActivity.this, ListViewActivity.class);
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.MONSTERS.getValue());
                break;
            default:
                intent = null;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }
}
