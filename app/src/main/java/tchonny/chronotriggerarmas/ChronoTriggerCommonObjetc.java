package tchonny.chronotriggerarmas;

import java.io.Serializable;

/**
 * Created by tchonny on 25/10/2017.
 */

public abstract class ChronoTriggerCommonObjetc implements Serializable {
    private String iD;

    public ChronoTriggerCommonObjetc(String iD) {
        this.iD = iD;
    }

    public String getiD() {
        return iD;
    }
}
