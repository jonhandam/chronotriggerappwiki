package tchonny.chronotriggerarmas;

/**
 * Created by Jonatas on 19/10/2017.
 */

public class Technique extends ChronoTriggerCommonObjetc {
    String techIdImage, description, mp, requirementsOrTp, damageType, target, characters, acessoryRequired;

    public Technique(String techIdImage, String iD, String mp, String requirementsOrTp,
                     String target, String damageType, String description,
                     String acessoryRequired, String characters) {
        super(iD); //nameOfTech
        this.techIdImage = techIdImage;
        this.characters = characters;
        this.description = description;
        this.mp = mp;
        this.damageType = damageType;
        this.target = target;
        this.requirementsOrTp = requirementsOrTp;
        this.acessoryRequired = acessoryRequired;
    }



    public String getTechIdImage() {
        return techIdImage;
    }

    public String getCharacters() { return characters; }

    public String getDescription() {
        return description;
    }

    public String getMp() {
        return mp;
    }

    public String getDamageType() {
        return damageType;
    }

    public String getTarget() {
        return target;
    }

    public String getRequirementsOrTp() { return requirementsOrTp; }

    public String getAcessoryRequired() { return acessoryRequired; }
}
