package tchonny.chronotriggerarmas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class TechniqueActivity extends AppCompatActivity {

    private TextView techName, mpCost, requerimentsOrTp, target,
            description, charactersName, damageType, acessoryRequired;
    private TextView requerimentsOrTpTextView;
    private ImageView imageView;
    private Technique technique;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_tech);

        techName = findViewById(R.id.techNameValueID);
        mpCost = findViewById(R.id.techMPCostValueID);
        requerimentsOrTp = findViewById(R.id.techRequerimentsValueID);
        target = findViewById(R.id.techTargetValueID);
        description = findViewById(R.id.techDescriptionValueID);
        imageView = findViewById(R.id.techiniqueImageView);
        charactersName = findViewById(R.id.techCharactersNamesValueID);
        damageType = findViewById(R.id.techDamageTypeValueID);
        acessoryRequired = findViewById(R.id.techAcessoryRequiredValueID);

        requerimentsOrTpTextView = findViewById(R.id.techRequerimentsTextID);

        createSingleTechView ();
    }

    protected void createSingleTechView () {
        Intent intent = getIntent();
        Bundle extra = intent.getExtras();

        int techImageNumber ;
        String techType = extra.getString(ChronoTriggerID.ID.getValue()); ;

        if((extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.SINGLE_TECH.getValue()))) {
            requerimentsOrTpTextView.setText("Custo de TP");
            techImageNumber = R.array.single_tech;
        }
        else if ((extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.DUAL_TECH.getValue()))){
            techImageNumber = R.array.dual_tech;
            requerimentsOrTpTextView.setText("Requisitos");
        } else if((extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.TRIPLE_TECH.getValue()))) {
            techImageNumber = R.array.triple_tech;
            requerimentsOrTpTextView.setText("Requisitos");
//            technique = (Technique) intent.getSerializableExtra(ChronoTriggerID.TRIPLE_TECH.getValue());
//            ListGetter.setImageView(this, imageView, R.array.dual_tech, triple_tech.getTechIdImage());
        } else {
            techImageNumber = 0;
        }

        technique = (Technique) intent.getSerializableExtra(techType);
        techName.setText(technique.getiD());
        mpCost.setText(technique.getMp());
        target.setText(technique.getTarget());
        description.setText(technique.getDescription());
        charactersName.setText(technique.getCharacters());
        requerimentsOrTp.setText(technique.getRequirementsOrTp());
        damageType.setText(technique.getDamageType());
        acessoryRequired.setText(technique.getAcessoryRequired());

        ListGetter.setImageView(this, imageView, techImageNumber, technique.getTechIdImage());
    }
}
