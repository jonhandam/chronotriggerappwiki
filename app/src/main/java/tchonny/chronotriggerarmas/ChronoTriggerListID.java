package tchonny.chronotriggerarmas;

/**
 * Created by tchonny on 28/10/2017.
 */

public enum ChronoTriggerListID {
    ACESSORY(0), ARMOR(1), CHARACTER(2), DUAL_TECH(3), FINAL(4), GUIDE(5), HELMET(6), ITEM(7), SINGLE_TECH(8),
    TRIPLE_TECH(9), WEAPON(10);

    private int value;

    ChronoTriggerListID(int value) { this.value = value; }

    public int getValue () {
        return value;
    }


}
