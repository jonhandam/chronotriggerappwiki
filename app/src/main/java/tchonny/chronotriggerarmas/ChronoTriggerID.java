package tchonny.chronotriggerarmas;

/**
 * Created by tchonny on 25/10/2017.
 */

public enum ChronoTriggerID {
    ID("ID"), ID_CHARACTER("ID_CHARACTER"), HISTORY("HISTORY"), WEAPON("WEAPON"), ARMOR("ARMOR"), HELMET("HELMET"), ITEM("ITEM"), ACESSORY("ACESSORY"),
    FINALS("FINALS"), GUIDE("GUIDE"), MONSTERS("MONSTER"), SINGLE_TECH("SINGLE_TECH"), DUAL_TECH("DUAL_TECH"),
    TRIPLE_TECH("TRIPLE_TECH"), CHARACTERS("CHARACTERS"),

    // Enum of Characters
    CHRONO("CHRONO"), MARLE("MARLE"), LUCCA("LUCCA"), FROG("FROG"), ROBO("ROBO"), AYLA("AYLA"), MAGUS("MAGUS");

    private String value;

    ChronoTriggerID(String value) {
        this.value = value;
    }

    public String getValue () {
        return value;
    }
}
