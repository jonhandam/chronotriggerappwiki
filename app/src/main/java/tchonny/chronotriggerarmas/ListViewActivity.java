package tchonny.chronotriggerarmas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ListViewActivity extends AppCompatActivity /*implements ListView.OnItemClickListener*/{

    ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        listview = (ListView) findViewById(R.id.listViewID);
        Bundle extra = getIntent().getExtras();
        chooseListViewType(extra);
    }

    public void chooseListViewType (Bundle extra) {
        if (extra != null) {
            if(extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.ARMOR.getValue())) {
                createListView(ChronoTriggerID.ARMOR.getValue(),
                        ListGetter.getInstance().getArmorArrayList(this));

            } else if(extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.HELMET.getValue())) {
                createListView(ChronoTriggerID.HELMET.getValue(),
                        ListGetter.getInstance().getHelmetrArrayList(this));

            } else if(extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.ACESSORY.getValue())) {
                createListView(ChronoTriggerID.ACESSORY.getValue(),
                        ListGetter.getInstance().getAcessoryArrayList(this));

            } else if(extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.ITEM.getValue())) {
                createListView(ChronoTriggerID.ITEM.getValue(),
                        ListGetter.getInstance().getItemArrayList(this));

            } else if (extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.FINALS.getValue())) {
                createListView(ChronoTriggerID.FINALS.getValue(),
                        ListGetter.getInstance().getFinalsArrayList(this));

            } else if (extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.GUIDE.getValue())) {
                createListView(ChronoTriggerID.GUIDE.getValue(),
                        ListGetter.getInstance().getGuideArrayList(this));

            } else if (extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.SINGLE_TECH.getValue())) {
                createListView( ChronoTriggerID.SINGLE_TECH.getValue(),
                        ListGetter.getInstance().getSingleTechArrayList(this,
                                extra.get(ChronoTriggerID.ID_CHARACTER.getValue()).toString()) );

            } else if (extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.DUAL_TECH.getValue())) {
                createListView( ChronoTriggerID.DUAL_TECH.getValue(),
                        ListGetter.getInstance().getDualTechArrayList(this,
                                extra.get(ChronoTriggerID.ID_CHARACTER.getValue()).toString()) );

            } else if (extra.getString(ChronoTriggerID.ID.getValue()).equals(ChronoTriggerID.TRIPLE_TECH.getValue())) {
                createListView( ChronoTriggerID.TRIPLE_TECH.getValue(),
                        ListGetter.getInstance().getTripleTechArrayList(this,
                                extra.get(ChronoTriggerID.ID_CHARACTER.getValue()).toString()) );
            } else {
                //characterInfo(extra);
                createListView( ChronoTriggerID.WEAPON.getValue(),
                        ListGetter.getInstance().getWeaponArrayList(this,
                                extra.get(ChronoTriggerID.ID_CHARACTER.getValue()).toString()) );
            }
        }
    }

    public <T extends ChronoTriggerCommonObjetc> void createListView(final String typeOfList, final ArrayList<T> arrayListOfItem) {
        ArrayList<String> itemList = new ArrayList<String>();
        for (int i = 0; i < arrayListOfItem.size(); i++) {
            itemList.add(arrayListOfItem.get(i).getiD());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getApplicationContext(), android.R.layout.simple_list_item_1,
                        android.R.id.text1, itemList);

        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                if (typeOfList.equals(ChronoTriggerID.GUIDE.getValue())) {
                    intent = new Intent(ListViewActivity.this, ScrollViewActivity.class);
                } else if (typeOfList.equals(ChronoTriggerID.SINGLE_TECH.getValue())) {
                    intent = new Intent(ListViewActivity.this, TechniqueActivity.class);
                    //intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.SINGLE_TECH.getValue());
                }  else if (typeOfList.equals(ChronoTriggerID.DUAL_TECH.getValue())) {
                    intent = new Intent(ListViewActivity.this, TechniqueActivity.class);
                    //intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.DUAL_TECH.getValue());
                } else if (typeOfList.equals(ChronoTriggerID.TRIPLE_TECH.getValue())) {
                    intent = new Intent(ListViewActivity.this, TechniqueActivity.class);
                } else {
                    intent = new Intent(ListViewActivity.this, DescriptionViewActivity.class);
                }
                intent.putExtra(typeOfList, arrayListOfItem.get(position));
                intent.putExtra(ChronoTriggerID.ID.getValue(), typeOfList);
                startActivity(intent);
            }
        });
    }
}
