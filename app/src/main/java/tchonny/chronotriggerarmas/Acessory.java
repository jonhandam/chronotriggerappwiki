package tchonny.chronotriggerarmas;

/**
 * Created by tchonny on 01/09/2017.
 */

public class Acessory extends Item {
    private String user;

    public Acessory(String name, String ability, String whereToFind, String user) {
        super(name, ability, whereToFind);
        this.user = user;
    }

    public String getUser() { return user; }
}
