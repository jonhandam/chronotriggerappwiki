package tchonny.chronotriggerarmas;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button listButton, changeColorButton, techniqueButton, charactersButton,
            mapsButton, guideButton, historyButton, endGameButton;
    ImageView soundButton;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listButton = (Button) findViewById(R.id.listButtonID);
        changeColorButton = (Button) findViewById(R.id.changeColorButtonID);
        techniqueButton = (Button) findViewById(R.id.techniqueButtonID);
        charactersButton = (Button) findViewById(R.id.charactersButtonID);
        mapsButton = (Button) findViewById(R.id.mapButtonID);
        guideButton = (Button) findViewById(R.id.guideButtonID);
        historyButton = (Button) findViewById(R.id.historyButtonID);
        endGameButton = (Button) findViewById(R.id.endGameButton);

        soundButton = (ImageView) findViewById(R.id.soundButtonID);
        mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.frogs_theme);
        if (mediaPlayer != null) {
            //mediaPlayer.start();
        }

        listButton.setOnClickListener(this);
        changeColorButton.setOnClickListener(this);
        soundButton.setOnClickListener(this);
        techniqueButton.setOnClickListener(this);
        charactersButton.setOnClickListener(this);
        mapsButton.setOnClickListener(this);
        guideButton.setOnClickListener(this);
        historyButton.setOnClickListener(this);
        endGameButton.setOnClickListener(this);
    }

    protected void callSoundButtonAction() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                playSong();
            } else {
                pauseSong();
            }
        }
    }

    protected void playSong () {
        mediaPlayer.pause();
        soundButton.setImageResource(R.drawable.ic_volume_off_white_48dp);
    }
    protected void pauseSong () {
        mediaPlayer.start();
        soundButton.setImageResource(R.drawable.ic_volume_up_white_24dp);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.historyButtonID:
                intent = new Intent(MainActivity.this, ScrollViewActivity.class);
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.HISTORY.getValue());
                startActivity(intent);
                break;
            case R.id.charactersButtonID:
                intent = new Intent(MainActivity.this, CharactersChoiceActivity.class);
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.CHARACTERS.getValue());

                startActivity(intent);
                break;
            case R.id.techniqueButtonID:
                startActivity(new Intent(MainActivity.this, TechniquesTyoeChoiceActivity.class));
                break;
            case R.id.listButtonID:
                startActivity(new Intent(MainActivity.this, ItensListsActivity.class));
                break;
            case R.id.mapButtonID:
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
                break;
            case R.id.guideButtonID:
                intent = new Intent(MainActivity.this, ListViewActivity.class);
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.GUIDE.getValue());
                startActivity(intent);
                break;
            case R.id.endGameButton:
                intent = new Intent(MainActivity.this, ListViewActivity.class);
                intent.putExtra(ChronoTriggerID.ID.getValue(), ChronoTriggerID.FINALS.getValue());
                startActivity(intent);
                break;
            case R.id.changeColorButtonID:
                startActivity(new Intent(MainActivity.this, ChangeColorActivity.class));
                break;
            /*case R.id.soundButtonID:
                callSoundButtonAction();
                break; */
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
    }
}
